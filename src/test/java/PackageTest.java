import packages.PAuthorization;
import packages.PackageManager;
import packages.PackageType;

/**
 * Created by STrafe on 24.04.14.
 */
public class PackageTest {
    public static void start() throws Exception {
        PAuthorization.Body.Builder builder = PAuthorization.Body.newBuilder();
        builder.setAuthType(PAuthorization.Body.AuthType.VK);
        builder.setKey("key value :)");
        builder.setPassword("123456789867654321");
        byte[] packBytes = builder.build().toByteArray();

        byte[] netBytes = PackageManager.getNetworkBytes(PackageType.DMS_PACK_AUTHORIZATION, packBytes);
        Object o = PackageManager.getPackage(netBytes);

    }
}

package newtork;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.nio.ByteBuffer;

/**
 * Created with IntelliJ IDEA.
 * By: STr
 * Date: 05.11.13
 * Time: 15:40
 */

public class WebSocketServer {
    private final int port;

    private io.netty.channel.Channel mainChannel;

    public WebSocketServer(int port) {
        this.port = port;
    }

    public void run() throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        byte[] b = ByteBuffer.allocate(10).putShort((short) 1695).array();

        try {
            ServerBootstrap networkServer = new ServerBootstrap();
            networkServer.group(bossGroup, workerGroup);
            networkServer.channel(NioServerSocketChannel.class);
            networkServer.childHandler(new WebSocketServerInitializer());
            mainChannel = networkServer.bind(port).sync().channel();
            System.out.println("WebSocket socket server started at port " + port + '.');
            mainChannel.closeFuture().sync();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}

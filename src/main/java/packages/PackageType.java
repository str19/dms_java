package packages;

/**
 * Created by STrafe on 21.04.14.
 */
public enum PackageType {
    DMS_PACK_UNKNOWN,
    DMS_PACK_AUTHORIZATION,
    DMS_PACK_AUTHENTICATION
}

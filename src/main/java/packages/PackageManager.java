package packages;

import com.google.protobuf.InvalidProtocolBufferException;

import java.util.Arrays;

/**
 * Created by STr on 21.04.14.
 */
public class PackageManager {

    private static short readShort(byte[] data, int offset) {
        return (short) (((data[offset] << 8)) | ((data[offset + 1] & 0xff)));
    }

    private static byte[] shortToByteArray(short s) {
        return new byte[] { (byte) ((s & 0xFF00) >> 8), (byte) (s & 0x00FF) };
    }

    public static byte[] getNetworkBytes(PackageType packType, byte[] packBytes){
        byte[] packIdBytes = shortToByteArray((short) packType.ordinal());
        byte[] result = new byte[packIdBytes.length + packBytes.length];
        for (int i=0; i < packIdBytes.length; i++){
            result[i] = packIdBytes[i];
        }
        for (int i=0; i < packBytes.length; i++){
            result[packIdBytes.length + i] = packBytes[i];
        }
        return result;
    }

    public static Object getPackage(byte[] bytes) throws Exception {
        byte[] packageIdBytes = Arrays.copyOfRange(bytes, 0, 2);
        byte[] packageBytes = Arrays.copyOfRange(bytes, 2, bytes.length);

        short packageId = readShort(packageIdBytes, 0);
        PackageType packageType;
        try {
            packageType = PackageType.values()[packageId];
        }
        catch (Exception e) {
            throw new Exception("package not recognized");
        }
        Object o = null;
        switch (packageType){
            case DMS_PACK_UNKNOWN:
                throw new Exception("package type is " + PackageType.DMS_PACK_UNKNOWN.toString());
            case DMS_PACK_AUTHORIZATION:
                o = createAuthorizationPackage(packageBytes);
                break;
            case DMS_PACK_AUTHENTICATION:
                o = createAuthenticationPackage(packageBytes);
                break;
        }
        return o;
    }

    private static PAuthorization.Body createAuthorizationPackage(byte[] bytes) throws InvalidProtocolBufferException {
        PAuthorization.Body body = PAuthorization.Body.parseFrom(bytes);
        return body;
    }

    private static PAuthentication.Body createAuthenticationPackage(byte[] bytes) throws InvalidProtocolBufferException {
        PAuthentication.Body body = PAuthentication.Body.parseFrom(bytes);
        return body;
    }
}

import java.io.File;
import java.net.URL;

public class Main {
	public static int port = 8080;
	public static URL dbconfig;
	private enum ArgNames {
		port,
		dbconfig
	}

    public static void main(String[] args) throws Exception {
		for (String arg : args){
			String d = "=";
			int di = arg.indexOf(d);
			String argN = arg.substring(0,di).trim();
			String argV = arg.substring(di+d.length(), arg.length()).trim();

			ArgNames an;

			try {
				an = ArgNames.valueOf(argN);
			} catch (Exception e){
				throw new Exception("RUN FAIL: unsupporded argument- " + argN);
			}


			switch (an){
				case port:
					port = Integer.parseInt(argV);
					break;
				case dbconfig:
					dbconfig = new File(argV).toURI().toURL();
					break;
			}
		}

		if (dbconfig == null){
			throw new Exception("RUN FAIL: parameter dbconfig is empty");
		}

		Starter.start();
    }
}

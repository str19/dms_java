import newtork.WebSocketServer;

/**
 * Created by STr on 05.04.14.
 */
public class Starter {
    public static void start() throws Exception {
        new WebSocketServer(Main.port).run();
    }
}
var sock = null;

document.connect = function(){
    sock = new WebSocket("ws://192.168.1.200:8080");
//    sock = new WebSocket("ws://162.243.20.40:8080/websocket");
    sock.onopen = function() {
        showMessage('connection open');
    };
    sock.onmessage = function(e) {
        showMessage(e.data);
    };
    sock.onclose = function() {
        showMessage('connection close');
    };
};

document.disconnect = function(){
    if (sock != null) sock.close();
};

document.forms.publish.onsubmit = function() {
    if (sock == null){
        alert("state is disconected");
        return false;
    }
    var sendingObject = {
        "requesttype": 'click',
        "uid": this.uid.value,
        "token": this.token.value
    };
    sock.send(encoding.GetBytes(JSON.stringify(sendingObject)));

    return false;
};

function showMessage(message) {
    message = "server say: " + message;
    var messageElem = document.createElement('div');
    messageElem.appendChild(document.createTextNode(message));
    document.getElementById('log').appendChild(messageElem);
    console.log(message);
};

document.clear = function (){
    document.getElementById('log').innerHTML = '';
};